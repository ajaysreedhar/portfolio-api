# Portfolio API

A portfolio tracking API which allows adding, updating and deleting trades. 

## Features

- Implements minimal micro-service architecture.
- Service discovery with [Seneca toolkit](http://senecajs.org/) using [Seneca mesh](https://github.com/senecajs/seneca-mesh). 
- Uses MongoDB for document store.
- Uses Mongoose for MongoDB object modelling. 
 
## Installing and running

Requires node v4.8.3 or higher and npm 2.15.11 or higher.

Clone the repository:
```bash
$ git clone https://ajaysreedhar@bitbucket.org/ajaysreedhar/portfolio-api.git 
```

### Running as Docker container

Build the Docker image:
```bash
$ docker build -t portfolio-api .
```

Run the container:
```bash
$ docker run --name=portfolio-test -p 80:80 \
  -e "mongohost=mongodb://host:port/database_name" \
  -e "mongouser=user_name" \
  -e "mongopass=password" \ 
  -d portfolio-api:latest
```

### Running within the shell

Install dependencies:
```bash
$ cd portfolio-api
$ npm install 
```

Export MongoDB connection parameters:
```bash
$ export mongohost="mongodb://host:port/database_name"
$ export mongouser=user_name
$ export mongopass=mongo_password
```

Run the services:
```bash
$ chmod a+x start.sh
$ ./start.sh
```

(Optional) Run ESLint:
```bash
$ npm test
```

## API endpoints

By default the gateway listens on port number 80 (may require super user permissions).
The APIs accepts and produces **application/json**.

The following API end-points are provided:

### View server information
```
GET /
```

Sample response (HTTP 200 OK):
```json
{
  "version": "1.0.0",
  "description": "portfolio-api",
  "platform": "linux",
  "node": {
    "version": "v4.8.3"
  }
}
```

### Add a new portfolio
```
POST /portfolios
```

Sample request:
```json
{
	"name": "Example Portfolio"
}
```

Response (HTTP 201 Created): 
```json
{
  "success": true,
  "data": {
    "__v": 0,
    "name": "Example Portfolio",
    "_id": "5963864fab859168b2504fc6"
  }
}
```

### List portfolios
```
GET /portfolios?limit=100&skip=1
```

Sample response (HTTP 200 OK): 
```json
{
  "success": true,
  "data": [{
    "_id": "5963864fab859168b2504fc6",
    "name": "Example Portfolio"
  },
  {
    "_id": "5963864fab859168b2504fd5",
    "name": "Next Portfolio"
  }]
}
```

### View a particular portfolio
```
GET /portfolios/:portfolioId
```

Sample response (HTTP 200 OK): 
```json
{
  "success": true,
  "data": [
    {
      "_id": "5963864fab859168b2504fc6",
      "name": "Example Portfolio",
      "stocks": [
        {
          "_id": "596392a8f352ef000b7360d4",
          "name": "RELIANCE",
          "trades": [
            {
              "_id": "59639a4f27d47700096b6b4b",
              "type": "BUY",
              "price": 900,
              "quantity": 100,
              "timestamp": "2017-07-10T15:16:31.013Z"
            },
            {
              "_id": "59639a6d27d47700096b6b4c",
              "type": "SELL",
              "price": 1000,
              "quantity": 50,
              "timestamp": "2017-07-10T15:17:01.218Z"
            },
            {
              "_id": "59639a7d27d47700096b6b4d",
              "type": "BUY",
              "price": 850,
              "quantity": 100,
              "timestamp": "2017-07-10T15:17:17.252Z"
            }
          ]
        },
        {
          "_id": "596392eef352ef000b7360d5",
          "name": "HDFCBANK",
          "trades": [
            {
              "_id": "59639ac027d47700096b6b4e",
              "type": "BUY",
              "price": 1000,
              "quantity": 200,
              "timestamp": "2017-07-10T15:18:24.072Z"
            },
            {
              "_id": "59639ad727d47700096b6b4f",
              "type": "SELL",
              "price": 800,
              "quantity": 100,
              "timestamp": "2017-07-10T15:18:47.174Z"
            }
          ]
        }
      ]
    }
  ]
}
```

### List holdings with average buying price
```
GET /portfolios/:portfolioId/holdings
```

Sample response (HTTP 200 OK): 
```json
{
  "success": true,
  "data": [
    {
      "_id": "5963864fab859168b2504fc6",
      "name": "Example Portfolio",
      "stocks": [
        {
          "_id": "596392a8f352ef000b7360d4",
          "name": "RELIANCE",
          "avgBuyingPrice": 875
        },
        {
          "_id": "596392eef352ef000b7360d5",
          "name": "HDFCBANK",
          "avgBuyingPrice": 1000
        }
      ]
    }
  ]
}
```

### Add a new stock to a portfolio
```
POST /portfolios/:portfolioId/stocks
```

Sample request:
```json
{
	"name": "RELIANCE"
}
```

Response (HTTP 201 Created): 
```json
{
  "success": true,
  "data": {
    "__v": 0,
    "portfolio": "5963864fab859168b2504fc6",
    "name": "RELIANCE",
    "_id": "596392a8f352ef000b7360d4"
  }
}
```

### List stocks in a portfolio
```
GET /portfolios/:portfolioId/stocks?limit=100&skip=0
```

Sample response (HTTP 200 OK): 
```json
{
  "success": true,
  "data": [
    {
      "_id": "596392a8f352ef000b7360d4",
      "name": "RELIANCE"
    },
    {
      "_id": "596392eef352ef000b7360d5",
      "name": "HDFCBANK"
    }
  ]
}
```

### Calculate cumulative return of a stock
```
GET /stocks/:stockId/returns
```

Sample response (HTTP 200 OK): 
```json
{
  "success": true,
  "data": [
    {
      "_id": "596392a8f352ef000b7360d4",
      "name": "RELIANCE",
      "initialPrice": 900,
      "current": 1533,
      "return": 0.7033333333333334
    }
  ]
}
```

### Add a new trade to a stock
```
POST /stocks/:stockId/trades
```

Sample request:
```json
{
	"type": "BUY",
	"price": 900,
	"quantity": 100,
	"timestamp": "2017-07-06"
}
```
_The timestamp field is optional. Current timestamp is used if not provided._

Response (HTTP 201 Created):
```json
{
  "success": true,
  "data": {
    "__v": 0,
    "stock": "596392a8f352ef000b7360d4",
    "type": "BUY",
    "price": 900,
    "quantity": 100,
    "timestamp": "2017-07-10T15:05:58.867Z",
    "_id": "596397d627d47700096b6b4a"
  }
}
```

### List trades of a stock
```
GET /stocks/:stockId/trades?limit=100&skip=0
```

Sample response (HTTP 200 OK):
```json
{
  "success": true,
  "data": [
    {
      "_id": "596397d627d47700096b6b4a",
      "type": "BUY",
      "price": 900,
      "quantity": 100,
      "timestamp": "2017-07-10T15:05:58.867Z"
    }
  ]
}
```

### Update a trade 
```
PATCH /trades/:tradeId
```

Sample request:
```json
{
	"quantity": 110
}
```

Response (HTTP 200 OK):
```json
{
  "success": true,
  "updated": {
    "ok": 1,
    "nModified": 1
  }
}
```

### Delete a trade
```
DELETE /trades/:tradeId
```

Response (HTTP 200 OK):
```json
{
  "success": true
}
```