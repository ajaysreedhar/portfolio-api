/**
 * Displays friendly errors.
 *
 * @param error mongoose error object
 * @returns {{error: {title: string, code: number, details: string}}}
 */
module.exports = function (error) {

    var eObj = {
        title: 'Internal server error',
        code: 500,
        details: 'An error occurred while processing the request!'
    };

    if (typeof error === 'string') {
        eObj.title = 'An error occurred';
        eObj.code = 500;
        eObj.details = error;

        return { error: eObj };
    }

    if ( typeof error === 'object') {
        if (!error) {
            eObj.title = 'Unknown error';
            eObj.code = 'An unknown error has occurred!';
            eObj.details = error;

            return { error: eObj };
        }

        if (typeof error.message === 'string') {
            eObj.details = error.message;

        } else if (typeof error.errmsg === 'string') {
            eObj.details = error.errmsg;
        }

        if ( typeof error.code === 'number') {
            switch (error.code) {
                case 11000:
                    eObj.title = 'Duplicate key error';
                    eObj.code = 400;
                    break;

                default:
                    break;
            }
        }
    }

    return { success: false, error: eObj };
};