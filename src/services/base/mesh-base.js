/**
 * Seneca mesh base
 */
(function (tag, port, process) {
    'use strict';

    const bases = (process.argv[4] || '').split(',');

    require('seneca')({
        tag: tag,
        log: 'silent',
        debug: { short_logs: false }
    }).use('mesh', {
        isbase: true,
        port: port,
        bases: bases,
        pin:'role:mesh'
    }).ready( () => {
        console.log('Base ' + tag + ' started.');
    });

})(process.argv[2], process.argv[3], process);