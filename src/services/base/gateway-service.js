'use strict';

const BASES = (process.argv[2] || '').split(',');

const hapi = require('hapi');
const wo = require('wo');

const Server = new hapi.Server();

Server.connection({ port: 80 });
Server.register({
    register: wo,
    options: {
        bases: BASES,
        sneeze: { silent: true }
    }
});

Server.route({
    method: 'GET',
    path: '/',
    handler: (request, reply) => {
        return reply({
            version: '1.0.0',
            description: 'portfolio-api',
            platform: process.platform,
            node: {
                version: process.version,
            }
        }).code(200);
    }
});

/* Routes to portfolio-service. */
Server.route({
    method: ['GET', 'POST'],
    path: '/portfolios',
    handler: { wo: { passThrough: true } }
});

Server.route({
    method: ['GET'],
    path: '/portfolios/{pfId}',
    handler: { wo: { passThrough: true } }
});

Server.route({
    method: ['GET'],
    path: '/portfolios/{pfId}/holdings',
    handler: { wo: { passThrough: true } }
});

/* Routes to stock-service. */
Server.route({
    method: ['GET', 'POST'],
    path: '/portfolios/{pfId}/stocks',
    handler: { wo: { passThrough: true } }
});

Server.route({
    method: ['GET'],
    path: '/stocks/{stockId}',
    handler: { wo: { passThrough: true } }
});

Server.route({
    method: ['GET'],
    path: '/stocks/{stockId}/returns',
    handler: { wo: { passThrough: true } }
});

/* Routes to trade-service. */
Server.route({
    method: ['GET', 'POST'],
    path: '/stocks/{stockId}/trades',
    handler: { wo: { passThrough: true } }
});

Server.route({
    method: ['GET', 'PATCH', 'DELETE'],
    path: '/trades/{tradeId}',
    handler: { wo: { passThrough: true } }
});

Server.start((err) => {
    if (err) {
        console.log(err);
        return;
    }

    console.log('Server running at: ' + Server.info.uri);
});