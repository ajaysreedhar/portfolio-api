'use strict';

const tag = 'trade-service';

const hapi = require('hapi');
const chairo = require('chairo');
const seneca = require('seneca');
const wo = require('wo');
const mongoose = require('mongoose');
const fs = require('fs');
const _ = require('underscore');

const friendly = require('../../lib/friendly-errors');

const Server = new hapi.Server();
const bases = (process.argv[2] || '').split(',');

mongoose.Promise = require('promise');

/**
 * Mongoose model for trades.
 */
var Trade;

try {
    /* Read and parse the mongoose schema .json file. */
    let file = __dirname + '/trade-schema.json';
    let schema = JSON.parse(fs.readFileSync(file, 'utf-8'));

    /* Connect to MongoDB. */
    mongoose.connect(process.env.mongohost || 'mongodb://172.17.0.2:27017/portfolio_db/', {
        user: process.env.mongouser || '',
        pass: process.env.mongopass || ''
    });

    Trade = mongoose.model('trade_list', new mongoose.Schema(schema, { collection: 'trade_list' }));

} catch (e) {
    console.log(e);
    process.exit(1);
}

/**
 * Records a new trade in the database collection.
 *
 * @param payload request payload
 * @param reply Hapi reply method
 */
var insert = (payload, reply) => {
    if (!payload || Object.keys(payload).length <= 1) {
        reply({ success: false, error: 'Invalid or no payload' }).code(400);
        return;
    }

    /* Use current timestamp if not provided in the request. */
    if (typeof payload.timestamp === 'undefined') {
        payload.timestamp = new Date();

    } else {
        payload.timestamp = new Date(payload.timestamp);
    }

    let td = new Trade(payload);

    td.save((error, data) => {
        if (error) {
            let fe = friendly(error);
            reply(fe).code(fe.error.code);
            return;
        }

        reply({ success: true, data: data }).code(201);
    });
};

/**
 * Retrieves the details of a trade identified by a query.
 *
 * @param query object ID of the trade
 * @param reply Hapi reply method
 */
var retrieve = (query, reply) => {
    let match = ((typeof query === 'object') ? query : { _id: query });
    let paginate = { skip: 0, limit: 10};

    if (!isNaN(match.limit) && parseInt(match.limit) <= 100) {
        paginate.limit = parseInt(match.limit);
        delete match.limit;
    }

    if (!isNaN(match.skip)) {
        paginate.skip = parseInt(match.skip);
        delete match.skip;
    }

    Trade.find(match, ['_id', 'type', 'quantity', 'price', 'timestamp'], paginate, (error, trades) => {
        if (error) {
            let e = friendly(error);
            reply(e).code(e.error.code);
            return;
        }

        reply({ success: true, data: trades}).code(200);
    });
};

/**
 * Updates details of a trade matched by the ID with the given payload.
 *
 * @param id trade ID
 * @param payload new details
 * @param reply Hapi reply method
 */
var update = (id, payload, reply) => {
    if (!payload || Object.keys(payload).length <= 0) {
        reply({ error: 'Invalid or no payload' }).code(400);
        return;
    }

    Trade.update({ _id: id }, payload, (error, data) => {
        if (error) {
            let e = friendly(error);

            reply(e).code(e.error.code);
            return;
        }

        reply({ success: true, updated: data }).code(200);
    });
};

/**
 * Deletes a trade matched by the ID.
 *
 * @param id trade ID
 * @param reply Hapi reply method
 */
var remove = (id, reply) => {
    Trade.findOneAndRemove({ _id: id}, (error) => {

        if (error) {
            let e = friendly(error);
            reply(e).code(e.error.code);
            return;
        }

        reply({ success: true }).code(200);
    });
};

Server.connection({ port: 0 });

/* Chairo integrates Seneca and Hapi. */
Server.register({
    register: chairo,
    options: {
        seneca: seneca({ tag: tag, log: 'silent', debug: {short_logs:false} })
    }
});

/* Register proxy handler. */
Server.register({
    register: wo,
    options:{
        bases: bases,
        route: [
            { method: ['GET', 'POST'], path: '/stocks/{stockId}/trades' },
            { method: ['GET', 'PATCH', 'DELETE'], path: '/trades/{tradeId}' }
        ],
        sneeze: { silent: true }
    }
});

/* Server routes to list and create trades.
 * These actions requires a stock ID. */
Server.route({
    method: ['GET', 'POST'],
    path: '/stocks/{stockId}/trades',
    handler: (request, reply) => {

        /* Obtain stock ID in the request parameter. */
        let stockId = encodeURIComponent (request.params.stockId);

        switch (request.method) {
            case 'get':
                retrieve(_.extend({ stock: stockId }, (request.query || {})), reply);
                break;

            case 'post':
                insert(_.extend({ stock: stockId }, (request.payload || {})), reply);
                break;

            default:
                reply({ success: false, error: 'HTTP method not supported.' }).code(405);
                break;
        }
    }
});

/* Server routes to view, update and delete trades.
 * These actions requires a trade ID. */
Server.route({
    method: ['GET', 'PATCH', 'DELETE'],
    path: '/trades/{tradeId}',
    handler: (request, reply) => {

        /* Obtain ID in the request parameter. */
        let tradeId = encodeURIComponent(request.params.tradeId);

        switch (request.method) {
            case 'get':
                retrieve(tradeId, reply);
                break;

            case 'patch':
                update(tradeId, request.payload, reply);
                break;

            case 'delete':
                remove(tradeId, reply);
                break;

            default:
                reply({ success: false, error: 'HTTP method not supported.' }).code(405);
                break;
        }
    }
});

Server.seneca.use('mesh', { base: bases });

Server.start( (error) => {
    if (error) return;

    console.log(tag + ' started: ' + Server.info.uri);
});

