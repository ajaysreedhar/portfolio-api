'use strict';

const tag = 'stock-service';

const hapi = require('hapi');
const chairo = require('chairo');
const seneca = require('seneca');
const wo = require('wo');
const mongoose = require('mongoose');
const unirest = require('unirest');
const fs = require('fs');
const _ = require('underscore');

const friendly = require('../../lib/friendly-errors');

const Server = new hapi.Server();
const bases = (process.argv[2] || '').split(',');

mongoose.Promise = require('promise');

/**
 * Mongoose model for stocks.
 */
var Stock;

try {
    /* Read and parse the mongoose schema .json file. */
    let file = __dirname + '/stock-schema.json';
    let schema = JSON.parse(fs.readFileSync(file, 'utf-8'));

    /* Connect to MongoDB. */
    mongoose.connect(process.env.mongohost || 'mongodb://172.17.0.2:27017/portfolio_db/', {
        user: process.env.mongouser || '',
        pass: process.env.mongopass || ''
    });

    Stock = mongoose.model('stock_list', new mongoose.Schema(schema, { collection: 'stock_list' }));

} catch (e) {
    console.log(e);
    process.exit(1);
}

/**
 * Inserts a new stock into the database collection.
 *
 * @param payload request payload
 * @param reply Hapi reply method
 */
var insert = (payload, reply) => {
    if (!payload || Object.keys(payload).length <= 1) {
        reply({ success: false, error: 'Invalid or no payload' }).code(400);
        return;
    }

    let sk = new Stock(payload);

    sk.save((error, data) => {
        if (error) {
            let fe = friendly(error);
            reply(fe).code(fe.error.code);
            return;
        }

        reply({ success: true, data: data }).code(201);
    });
};

/**
 * Retrieves the details of a stock identified by a match.
 *
 * @param query match
 * @param reply Hapi reply method
 */
var retrieve = (query, reply) => {
    let match = ((typeof query === 'object') ? query : { _id: query });
    let paginate = { skip: 0, limit: 10};

    if (!isNaN(match.limit) && parseInt(match.limit) <= 100) {
        paginate.limit = parseInt(match.limit);
        delete match.limit;
    }

    if (!isNaN(match.skip)) {
        paginate.skip = parseInt(match.skip);
        delete match.skip;
    }

    Stock.find(match, ['_id', 'name'], paginate, (error, stocks) => {
        if (error) {
            let e = friendly(error);
            reply(e).code(e.error.code);
            return;
        }

        reply({ success: true, data: stocks}).code(200);
    });
};

/**
 * Calculates the cumulative return.
 *
 * @param id stock ID
 * @param reply Hapi reply method
 */
var cumulative = (id, reply) => {

    /* Use MongoDB aggregation pipelines to retrieve
     * the earliest occurred trade of the stock. */
    Stock.aggregate([
        { '$match': { _id: mongoose.Types.ObjectId(id) }},
        { '$lookup': {
            from: 'trade_list',
            localField: '_id',
            foreignField: 'stock',
            as: 'trades'
        } },
        { '$project': {
            _id: true,
            name: true,
            trades: {
                '$map': {
                    input: '$trades',
                    as: 'item',
                    in: { '$cond': [{'$eq': ['$$item.type', 'BUY']},'$$item', null] }
                }
            }
        } },
        { '$unwind' : { path: '$trades', preserveNullAndEmptyArrays: false } },
        { '$match': { 'trades': { '$ne': null } } },
        { '$sort': { 'trades.timestamp': 1 } },
        { '$limit': 1 },
        { '$group': { '_id': '$_id', name: { '$first': '$name' }, trades: { '$push': '$trades' } } }
    ]).exec((error, stock) => {

        /* Handle errors. */
        if (error) {
            reply({ success: false, error: error }).code(500);
            return;
        }

        if (Array.isArray(stock) && stock.length <= 0) {
            reply({ success: false, error: 'A stock with ID ' + id + ' does not exist.' }).code(404);
            return;
        }

        stock = stock[0];

        /* Stop processing if the stock does not have any trades. */
        if (typeof stock.trades !== 'object' || stock.trades === null || stock.trades.length <= 0) {
            reply({ success: false, error: 'The selected stock does not contain any trades.' }).code(404);
            return;
        }

        let url = 'http://finance.google.com/finance/info?client=ig&q=' + stock.name;
        let trade = stock.trades[0];

        /* Use Unirest to query Google finance API
         * with the stock name as a query parameter. */
        unirest.get(url)
            .headers({'Accept': 'text/html'})
            .end((response) => {

                /* Abort if Google finance did not give a 200 OK status. */
                if (parseInt(response.code) !== 200) {
                    reply({
                        'success': false,
                        'error': 'Google finance API responded with HTTP ' + response.code + ' code.',
                        'details': response.raw_body,
                        'url': url
                    }).code(500);
                    return;
                }

                if (typeof response.raw_body !== 'string') {
                    return;
                }

                try {
                    /* Though the Finance API response is formatted as JSON, the Content-Type header
                     * is text/html. Hence Unirest will not parse the response automatically and should
                     * be done explicitly. */
                    let body = JSON.parse(response.raw_body.replace('// [', '').replace(']', ''));

                    /* Obtain the current price. The finance API response may contain
                     * commas. Remove the commas to parse without exceptions. */
                    let current = parseFloat(body.l.replace(',', ''));

                    /* Obtain initial trading price from MongoDB response. */
                    let initial = parseFloat(trade.price);

                    /* Return = (Current price – Initial price) / Initial price */
                    let cr = (current - initial )/initial;

                    delete stock.trades;

                    stock.initialPrice = initial;
                    stock.currentPrice = current;
                    stock.cumulativeReturn = cr;

                    reply({ success: true, data:[stock] });

                } catch (e) {
                    reply({ success: false, error: 'An error occurred while parsing the Google Finance API response.' }).code(500);
                }
            });
    });
};

Server.connection({ port: 0 });

/* Chairo integrates Seneca and Hapi. */
Server.register({
    register: chairo,
    options: {
        seneca: seneca({ tag: tag, log: 'silent', debug: {short_logs:false} })
    }
});

/* Registers proxy handler. */
Server.register({
    register: wo,
    options:{
        bases: bases,
        route: [
            { method: ['GET', 'POST'], path: '/portfolios/{pfId}/stocks'},
            { method: ['GET'], path: '/stocks/{stockId}'},
            { method: ['GET'], path: '/stocks/{stockId}/returns'}
        ],
        sneeze: { silent: true }
    }
});

/* Server routes to list create stocks.
 * These actions requires portfolio ID. */
Server.route({
    method: ['GET', 'POST'],
    path: '/portfolios/{pfId}/stocks',
    handler: (request, reply) => {
        let pfId = encodeURIComponent (request.params.pfId);

        switch (request.method) {
            case 'get':
                retrieve(_.extend({ portfolio: pfId }, (request.query || {})), reply);
                break;

            case 'post':
                insert(_.extend({ portfolio: pfId }, (request.payload || {})), reply);
                break;

            default:
                reply({ success: false, error: 'HTTP method not supported.' }).code(405);
                break;
        }
    }
});

/* Server route to view a stock. */
Server.route({
    method: ['GET'],
    path: '/stocks/{stockId}',
    handler: (request, reply) => {
        let stockId = encodeURIComponent (request.params.stockId);

        retrieve(stockId, reply);
    }
});

Server.route({
    method: ['GET'],
    path: '/stocks/{stockId}/returns',
    handler: (request, reply) => {
        let stockId = encodeURIComponent (request.params.stockId);

        cumulative(stockId, reply);
    }
});

Server.seneca.use('mesh', { base: bases });

Server.start( (error) => {
    if (error) return;

    console.log(tag + ' started: ' + Server.info.uri);
});

