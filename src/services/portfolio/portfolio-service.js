'use strict';

const tag = 'portfolio-service';

const hapi = require('hapi');
const chairo = require('chairo');
const seneca = require('seneca');
const wo = require('wo');
const mongoose = require('mongoose');
const fs = require('fs');

const friendly = require('../../lib/friendly-errors');

const Server = new hapi.Server();
const bases = (process.argv[2] || '').split(',');

mongoose.Promise = require('promise');

/**
 * Mongoose model for portfolios.
 */
var Portfolio;

try {
    /* Read and parse the mongoose schema .json file. */
    let file = __dirname + '/portfolio-schema.json';
    let schema = JSON.parse(fs.readFileSync(file, 'utf-8'));

    /* Connect to MongoDB. */
    mongoose.connect(process.env.mongohost || 'mongodb://172.17.0.2:27017/portfolio_db/', {
        user: process.env.mongouser || '',
        pass: process.env.mongopass || ''
    });

    Portfolio = mongoose.model('portfolio_list', new mongoose.Schema(schema, {collection: 'portfolio_list'}));

} catch (e) {
    console.log(e);
    process.exit(1);
}

/**
 * Inserts a new portfolio to the database collection.
 *
 * @param payload request payload
 * @param reply Hapi reply method
 */
var insert = (payload, reply) => {
    if (!payload || Object.keys(payload).length <= 0) {
        reply({ success: false, error: 'Invalid or no payload' }).code(400);
        return;
    }

    let pf = new Portfolio(payload);

    pf.save((error, data) => {
        if (error) {
            let fe = friendly(error);
            reply(fe).code(fe.error.code);
            return;
        }

        reply({ success: true, data: data }).code(201);
    });
};

/**
 * Retrieves a portfolio identified by the query.
 *
 * @param query match query
 * @param reply Hapi reply method
 */
var retrieve = (query, reply) => {
    let match = ((typeof query === 'object') ? query : { _id: query });
    let paginate = { skip: 0, limit: 10};

    if (!isNaN(match.limit) && parseInt(match.limit) <= 100) {
        paginate.limit = parseInt(match.limit);
        delete match.limit;
    }

    if (!isNaN(match.skip)) {
        paginate.skip = parseInt(match.skip);
        delete match.skip;
    }

    Portfolio.find(match, ['_id', 'name'], paginate, (error, portfolios) => {
        if (error) {
            let e = friendly(error);
            reply(e).code(e.error.code);
            return;
        }

        reply({ success: true, data: portfolios}).code(200);
    });
};

/**
 * Produces a aggregate view for the portfolio,
 * including stocks and trades.
 *
 * @param id portfolio ID
 * @param reply Hapi reply method
 */
var aggregate = (id, reply) => {
    Portfolio.aggregate([
        { '$match': { _id: mongoose.Types.ObjectId(id) }},
        { '$lookup': {
            from: 'stock_list',
            localField: '_id',
            foreignField: 'portfolio',
            as: 'stocks'
        } },
        { '$unwind': {
            path: '$stocks',
            preserveNullAndEmptyArrays: true
        } },
        { '$lookup': {
            from: 'trade_list',
            localField: 'stocks._id',
            foreignField: 'stock',
            as: 'stocks.trades'
        } },
        { '$project': {
            _id: true,
            name: true,
            stocks: {
                _id: true,
                name: true,
                trades: {
                    _id: true,
                    type: true,
                    quantity: true,
                    price: true,
                    timestamp: true
                }
            }
        } },
        { '$group': {_id: '$_id', name: {'$first' : '$name'}, stocks: { '$push': '$stocks'} }}
    ]).exec((error, portfolio) => {
        reply({ success: true, data: portfolio }).code(200);
    });
};

/**
 * Retrieves average buying price for each stock.
 *
 * @param id portfolio ID
 * @param reply Hapi reply method
 */
var holdings = (id, reply) => {
    Portfolio.aggregate([
        { '$match': { _id: mongoose.Types.ObjectId(id) } },
        { '$lookup': {
            from: 'stock_list',
            localField: '_id',
            foreignField: 'portfolio',
            as: 'stocks'
        } },
        { '$unwind': {
            path: '$stocks',
            preserveNullAndEmptyArrays: true
        } },
        { '$lookup': {
            from: 'trade_list',
            localField: 'stocks._id',
            foreignField: 'stock',
            as: 'stocks.trades'
        } },
        { '$project': {
            _id: true,
            name: true,
            stocks: {
                _id: true,
                name: true,
                avgBuyingPrice: {
                    '$avg': {
                        '$map': {
                            input: '$stocks.trades',
                            as: 'item',
                            in: { '$cond': [{'$eq': ['$$item.type', 'BUY']},'$$item.price', null] }
                        }
                    }
                }
            }
        } },
        { '$group': {_id: '$_id', name: {'$first': '$name'}, stocks: { '$push': '$stocks'} }}
    ]).exec((error, portfolio) => {
        reply({ success: true, data: portfolio }).code(200);
    });
};

Server.connection({ port: 0 });

/* Chairo integrates Seneca and Hapi. */
Server.register({
    register: chairo,
    options: {
        seneca: seneca({ tag: tag, log: 'silent', debug: {short_logs:false} })
    }
});

/* Register proxy handler. */
Server.register({
    register: wo,
    options:{
        bases: bases,
        route: [
            { method: ['GET', 'POST'], path: '/portfolios' },
            { method: ['GET'], path: '/portfolios/{pfId}' },
            { method: ['GET'], path: '/portfolios/{pfId}/holdings' }
        ],
        sneeze: { silent: true }
    }
});

/* Server routes to list and create portfolios. */
Server.route({
    method: ['GET', 'POST'],
    path: '/portfolios',
    handler: (request, reply) => {
        switch (request.method) {
            case 'get':
                retrieve(request.query, reply);
                break;

            case 'post':
                insert(request.payload, reply);
                break;

            default:
                reply({ success: false, error: 'HTTP method not supported.' }).code(405);
                break;
        }
    }
});

/* Server route to view the entire portfolio. */
Server.route({
    method: ['GET'],
    path: '/portfolios/{pfId}',
    handler: (request, reply) => {
        let pfId = encodeURIComponent (request.params.pfId);
        aggregate(pfId, reply);
    }
});

/* Server route to view holdings. */
Server.route({
    method: ['GET'],
    path: '/portfolios/{pfId}/holdings',
    handler: (request, reply) => {
        let pfId = encodeURIComponent (request.params.pfId);
        holdings(pfId, reply);
    }
});

Server.seneca.use('mesh', { base: bases, host: 'localhost' });

Server.start( (error) => {
    if (error) return;

    console.log(tag + ' started at: ' + Server.info.uri);
});

