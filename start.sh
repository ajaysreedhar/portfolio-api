#!/bin/bash

echo "Terminating previous instances..."
pkill node
echo "Done!"

BASES="127.0.0.1:39000"
OPTS="--seneca.options.debug.undead=true --seneca.options.plugin.mesh.sneeze.silent=1"

node src/services/base/mesh-base.js mesh-base-1 39000 ${BASES} ${OPTS} &
sleep 1

node src/services/portfolio/portfolio-service.js ${BASES} ${OPTS} &
sleep 1

node src/services/stocks/stock-service.js ${BASES} ${OPTS} &
sleep 1

node src/services/trades/trade-service.js ${BASES} ${OPTS} &
sleep 1

node src/services/base/gateway-service.js ${BASES} ${OPTS} &
sleep 3